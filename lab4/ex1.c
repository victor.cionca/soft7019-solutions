// Working with structures

#include <stdio.h>

// Structure representing student information
struct student{
    char name[10];  // student name
    int grade;      // final grade
};

/**
 * Set the final grade field of a struct student variable
 * to the grade paramter.
 * The function modifies the struct student variable _in place_,
 * it doesn't return anything. Therefore the struct student variable
 * needs to be a pointer.
 */
void set_grade(struct student *v, int g){
   v->grade = g; // v is a pointer to a struct student, so we use "->" instead
                 // of "." to access the fields.
}

void main(void){
    struct student s;   // variable of type struct student
    int grade;

    // Read the student's name with scanf
    printf("Student name: ");
    scanf("%s", s.name); // Read the string into the name field of variable s
                         // s.name is an array of characters, so s.name is the
                         // address of the first character.
                         // We don't need to use the & operator.

    printf("Final grade: ");
    scanf("%d", &grade); // Read the final grade into a local variable.
                         // Since grade is a scalar (not address) we need the &
                         // operator to get its address, which is then passed to
                         // scanf.
    set_grade(&s, grade);// The local variable grade is passed to the function
                         // which will assign it to the structure.
                         // set_grade takes as parameter the address of the 
                         // struct student variable so that it can modify the 
                         // grade field in the function.

    printf("Student name %s and grade %d\n", s.name, s.grade);
}
