// Library of functions for working with linked lists

#include <stdlib.h> // for malloc
#include "linkedlist.h" // definition of struct node and function prototypes

struct node *init_list(int value){
    struct node *n = malloc(sizeof(struct node));   // allocate memory for node
    n->content = value;     // Set the content of the node. Use -> since n is pointer
    n->next = NULL;         // Node is start and end of list so ->next is NULL
    return n;
}

struct node *append(struct node *head, int value){
    struct node *n = malloc(sizeof(struct node)); // allocate memory for new node
    n->content = value;
    n->next = NULL;

    // Go through the nodes in the list until the last node
    // The last node has ->next = NULL
    // We don't need an additional variable, we use the head parameter. Since
    // this is a copy (pass by value) of the actual address any changes we make 
    // to it (the address) are local only. Of course changing the memory contents
    // at the address (by dereferencing the pointer) will persist. As you see
    // below we only work with the address, we do not dereference it.
    while (head->next != NULL){
        head = head->next;
    }

    // We are at the last node, add the new node here
    head->next = n;

    return n;
}

struct node *add_after(struct node *n, int value){
    struct node *m = malloc(sizeof(struct node)); // allocate memory for new node
    m->content = value;

    /*
     * The new node m will be inserted between n and n->next.
     * This means we need n->next to be m, and m->next to be n->next.
     * A particular order must be followed so we don't lose nodes.
     */
    m->next = n->next; // First this because next we lose n->next
    n->next = m;

    return m;
}

void remove_after(struct node *n){
    /*
     * Remove the node n->next.
     * This means that n->next should be connected to n->next->next.
     * Again there is particular order to be followed in changing connections.
     * We have to keep track of the address of the node to be removed so 
     * we can free its memory.
     */
    struct node *toremove = n->next;
    n->next = n->next->next; // this disconnects toremove from the list
                             // Alternatively: n->next = toremove->next
    free(toremove);
}

int remove_first(struct node *head, int value){
    /*
     * Go through the nodes in the list until we find a node whose content is equal
     * to value, or until we get to the last node.
     * Keep track of the previous node so we can use remove_after above to 
     * remove the node with the right value.
     */
    struct node *c;
    struct node *previous;

    for (c = head, previous = NULL; c != NULL; previous = c, c = c->next){
        if (c->content == value){
            break;
        }
    }

    // Now we are in three possible situations:
    // 1. We are at the end of the list (c = NULL), which means the value was
    // not found, so we return -1
    if (c == NULL) return -1;

    // 2. The value was found at the head of the list.
    // This has two problems.
    // First, it means that previous = NULL, so we cannot use remove_after.
    // Second, and more important, if we remove the head node then the 
    // list will be lost after the call. With this function prototype it is
    // not possible to change the head of the list. 
    // We would need to either return the new head, or to receive the head
    // parameter as a pointer to pointer. The latter would allow us to update
    // the address of the head node.
    // So, we return -1 if it's the first node.
    if (c == head) return -1;

    // 3. Finally, the value is in the middle of the list so we have previous
    // as the address of the previous node. We can use remove_after.
    remove_after(previous);
    return 0;
}

// Clear the contents and release the memory of a list
void clear(struct node *head){
    while (head != NULL){
        struct node *c = head;  // Save the head pointer so we can free its memory
        head = head->next;      // Advance the head pointer to the next node
        free(c);                // Release memory of the previous node
    }
}
