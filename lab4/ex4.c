// List of numbers
// Since this uses the linked list library it must be compiled as
// gcc ex4.c linked_list.c -o ex4

#include <stdio.h>
#include "linkedlist.h"

void main(void){
    struct node *numbers = NULL;   // List of numbers
    int value;              // To read numbers
    char input = 0;         // Menu option
    int printmenu = 1;

    while (input != 'x'){
        // Menu
        if (printmenu){ // Only print the menu again if an action was executed
            printf("[1]\tRead number\n");
            printf("[2]\tPrint list\n");
            printf("[3]\tSum list\n");
            printf("[4]\tRemove first occurrence of number\n");
            printf("[x]\tExit\n");
        }
        input = getchar();
        printmenu = 1;
        switch (input){
            case '1': // read a number and add to the list
                printf("Number: "); scanf("%d", &value);
                if (numbers == NULL) numbers = init_list(value); // init list if empty
                else append(numbers, value); // append to the list
                break;
            case '2': // print the list
                {
                    struct node *c;
                    c = numbers;
                    printf("Numbers: ");
                    while (c != NULL){
                        printf("%d,", c->content);
                        c = c->next;
                    }
                    printf("\n");
                    break;
                }
            case '3': // sum the list
                {
                    struct node *c = numbers;
                    int sum = 0;
                    while (c != NULL){
                        sum += c->content;
                        c = c->next;
                    }
                    printf("Sum = %d\n", sum);
                    break;
                }
            case '4': // remove first occurrence of number
                printf("To remove: ");scanf("%d", &value);
                if (remove_first(numbers, value) == 0){
                    printf("Success\n");
                }else{
                    printf("Did not find value (or is first in list)\n");
                }
                break;
            default:
                printmenu = 0; // Don't print the menu again for unsupported options
                continue; // ignore other characters (such as newline)
        }
    }

    printf("Bye!\n");
}
