#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H

/**
 * This is a header file.
 * It contains definitions of types and function prototypes.
 */
struct node {
    int content;
    struct node *next;
};

struct node *init_list(int value);
struct node *append(struct node *head, int value);
struct node *add_after(struct node *n, int value);
void remove_after(struct node *n);
int remove_first(struct node *head, int value);
void clear(struct node *head);

#endif
