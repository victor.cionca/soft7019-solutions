// Manual linked list

#include <stdio.h>

// Singly linked list
struct node{
    int content;        // Content is integer variable
    struct node *next;  // Connection to next element.
};

void main(void){
    struct node n1 = {.content=1}; // Designated initialiser, only content is set
    struct node n2 = {.content=2}; // Designated initialiser, only content is set
    struct node n3 = {.content=3}; // Designated initialiser, only content is set
    struct node *c; // Cursor to walk the list

    // Connect the list. n1, n2, n3 are scalars so we use "." to access fields
    n1.next = &n2;
    n2.next = &n3;
    n3.next = NULL;

    for (c = &n1; c!=NULL; c=c->next){
        // c, the cursor starts with the address of the list head (n1)
        // It then moves through the nodes in the list using the next field.
        // Since c is a pointer to a struct we use -> to access the fields.
        printf("%d ", c->content);
    }
    printf("\n");
}
