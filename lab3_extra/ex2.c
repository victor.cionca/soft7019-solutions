// Calculator in Reverse Polish Notation
/*
 * We will read characters from the user with getchar.
 * This is more convenient than scanf because we will be getting numbers and
 * operators.
 * The numbers are separated by operators or commas.
 * Previously in lab 1 we implemented summing up a list of numbers in a similar
 * way. Now we have more operations so we will use an array as a stack to hold
 * the numbers until we get an operator.
 */

#include <stdio.h>

/**
 * Apply operator to the top two items on the stack
 * Push the result on the stack and update the top of stack
 */
void compute(int *stack, int *top, char operator){
    int arg1, arg2, result;
    (*top)--;
    arg1 = stack[*top]; // pop first argument
    (*top)--;
    arg2 = stack[*top]; // pop second argument
    switch (operator){
        case '+': result = arg2+arg1;break;
        case '-': result = arg2-arg1;break;
        case '*': result = arg2*arg1;break;
        case '/': result = arg2/arg1;break;
    }
    stack[*top] = result; // push the sum on the stack
    (*top)++;
}

void main(void){
    int stack[10];
    int top_of_stack = 0; // Need to keep track of the top of the stack
    char input = 0; // User input char, as usual
    int current_number = 0; // We will be building numbers from digits as in lab1
    int prev_is_digit = 0;  // Was the previous character a digit? See below.
    int i;

    while (input != '\n'){
        input = getchar();

        switch (input){
            case ',': // commas mark the end of a number, push number on the stack
                stack[top_of_stack] = current_number;
                top_of_stack++; // update the top of the stack
                current_number = 0; // reset the current number
                prev_is_digit = 0;
                printf("Stack: ");
                for (i=0;i<top_of_stack;i++) printf("%d|", stack[i]);printf("\n");
                break;
            case '+':
            case '-':
            case '*':
            case '/':
                /*
                 * For all the operators we perform roughly the same code,
                 * except for the actual operation, which is only one character
                 * in the code.
                 * Therefore we treat them all the same here and shift the code
                 * into a separate function.
                 * This makes the code more compact and easy to read and
                 * maintain.
                 *
                 * Without the break statement in the case labels of the other
                 * operators (above) the code will always end up here for all
                 * the operators.
                 */
                // First push the current number on the stack, as for the comma:
                // Since we can have operators coming right after operators
                // (ex: 2,4,5*+ = (4*5)+2)
                // in those cases we do not push the current number on the stack
                // because that would result in an extra 0 on the stack.
                // Therefore we keep track if the previous character was a digit,
                // which means that we just finished a number.
                if (prev_is_digit){
                    stack[top_of_stack] = current_number;
                    top_of_stack++; // update the top of the stack
                    current_number = 0; // reset the current number
                    prev_is_digit = 0;
                }
                printf("Stack: ");
                for (i=0;i<top_of_stack;i++) printf("%d|", stack[i]);printf("\n");
                compute(stack, &top_of_stack, input);
                break;
            default: // in the default case we have digits that we shift into 
                     // the current number
                if (input >= '0' && input <= '9'){
                    current_number *= 10;
                    current_number += input -'0';
                    prev_is_digit = 1;
                }
        }
    }
    /*
     * When we are done there will be a single element on the stack
     * and it will be the result of the computation.
     */
    printf("=%d. Top of stack=%d\n", stack[top_of_stack-1], top_of_stack-1);
}
