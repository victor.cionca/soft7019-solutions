// String tokeniser
#include <stdio.h>

int strtok(const char *string, char delimiter, char *token, char **state){
    // The state will hold the address of the string on subsequent calls.
    // It is an output function parameter, therefore the parameter
    // must be a pointer (a reference) to the address. A pointer to an address
    // is a pointer to pointer, hence char **state.
    int index = 0;

    // On first run when string is !NULL we save the address of the first char
    // of the string in the state pointer. Notice that we dereference state,
    // since state is a pointer to an address. To set the address in the state
    // we need to dereference the pointer.
    if (string != NULL) *state = string;

    /*
     * Two versions here.
     *
     * The first one, commented below is the straightforward one.
     * We go through the characters of the string with a loop until we either
     * reach the end of the string (the null terminator) or a delimiter.
     * We copy each character into the token.
     *
     * Notice how we access the characters in the string.
     * state is a pointer to the address of the start of the string.
     * Therefore the address at the start of the string is (*state).
     * The parantheses are necessary because the subscripting operator []
     * has priority over the dereferencing operator.
     */
    //while ((*state)[index] != '\0' && (*state)[index] != delimiter){
    //    token[index] = (*state)[index];
    //    index++;
    //}
    /*
     * The other version does it all in one line, in the condition of the while.
     * while ((*state)[index] != '\0'   // first part
     *          &&
     *        (token[index] = (*state)[index++]) != delimiter); // second part
     *
     * In the first part we check if the current character in the string is the
     * null terminator. In other words, if we are at the end of the string.
     *
     * The second part has:
     * 1. the assignment token[index] = (*state)[index++] assigns (*state)[index]
     *    to token[index], so copies the current character into the token
     * 2. the assignment evalutes to (*state)[index] which is the current character
     *    in the string that is compared to the delimiter to terminate the while
     *    loop when we get to a delimiter character in the string
     * 3. Finally index is incremented.
     */
    while ((*state)[index] != '\0' && (token[index] = (*state)[index++]) != delimiter);

    /*
     * If we use the one line version we end up with two possible values for index.
     * If we exit the while because we are at the end of the string (first part
     * of the condition) then index will be on the null terminator.
     * If we exit the while because we found a delimiter index will be at the
     * character after the delimiter (due to the index++).
     *
     * Therefore we need to handle these two cases as below when we mark the
     * end of the token.
     * In the first version (the one that uses the body of the while loop) this 
     * is not the case.
     */
    if ((*state)[index] == '\0') token[index] = '\0';
    else token[index-1] = '\0';

    /*
     * Now we update the state.
     * The state holds the address of the start of the string for the next call.
     * The next character in the string is at index, (*state)[index]. We need
     * to get its address with &.
     */
    *state = &((*state)[index]);
    return index;
}

void main(void){
    char user_string[20];
    char delim;
    char *state;
    char token[20];
    int toklen;

    printf("String: ");
    scanf("%s", user_string);
    getchar();
    printf("Delimiter: ");
    delim = getchar();
    getchar();

    // First call strtok with the user string and get the length of the token
    toklen = strtok(user_string, delim, token, &state);
    printf("%s\n", token);

    // Keep calling strtok with NULL as first parameter until toklen is 0
    while (toklen != 0){
        toklen = strtok(NULL, delim, token, &state);
        printf("%s\n", token);
    }
}
