#include <stdio.h>
#include <string.h>

void main(int argv, char *argc[]){
    int i; // use this to go through the argument list
    int j; // use this to go through the characters of an argument
    for (i=1;i<argv;i++){
        for (j=0;j<strlen(argc[i]);j++){ // j goes up to strlen of the argument
            switch (argc[i][j]){
                case 'a':case 'e':case 'i':case 'o':case 'u':
                    // We use passthrough case (no break) so we handle all
                    // vowels quickly in one place
                    printf("@");
                    break;
                default:
                    // Any other characters (default case) are printed out
                    printf("%c", argc[i][j]);
            }
        }
        printf("\n"); // newline after each argument
    }
}

