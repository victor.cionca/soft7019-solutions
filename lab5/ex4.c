#include <stdio.h>
#include <string.h>
#include "option_parser.h"

void main(int argc, char *argv[]){
    int options;
    struct parser *p = init_option_parser();
    add_option(p, "surname", 's', "surname", TYPE_STRING);
    add_option(p, "year", 'y', "year", TYPE_INT);
    add_option(p, "cs", 'c', "is_cs", TYPE_EMPTY);
    print_options(p);
    options = parse(p, argc, argv);

    struct option *surname = get_option(p, "surname");
    if (surname) printf("Surname: %s\n", surname->value.string_val);

    struct option *year = get_option(p, "year");
    if (year) printf("Year: %d\n", year->value.val);

    struct option *cs = get_option(p, "is_cs");
    if (cs && cs->value.set) printf("Restrict search to CS students\n");
}
