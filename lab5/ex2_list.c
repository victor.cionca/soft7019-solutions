#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "linkedlist.h"

// Declare a structure to store words and their lengths.
// This is what will go in the linked list
struct word{
    const char *word; // This will point to the command line arguments
    int word_len;
};

void main(int argv, char *argc[]){
    struct node *word_list = NULL;
    int i; // use this to go through the argument list
    for (i=1;i<argv;i++){
        // Prepare memory for the new word
        struct word *new_word = malloc(sizeof(struct word));
        new_word->word = argc[i];
        new_word->word_len = strlen(argc[i]);

        if (word_list == NULL){
            // If the list is empty initialise it with the first argument
            word_list = init_list(new_word);
        }else{
            // If the list is not empty we have to insert the new word
            // before the first word that is longer than it.
            // We will go through the list until we find the first word
            // that is longer than new_word.word_len.
            // We have to keep track of the previous node in the list, 
            // because our new word will be inserted after that previous
            // node.
            struct node *prev = NULL;
            struct node *crt = word_list;
            while (crt != NULL){
                // Obtain a struct word pointer to the contents of the
                // crt list node
                struct word *crt_word = (struct word*)crt->content;
                // If the current word in the list is longer,
                // then break out of the loop
                if (crt_word->word_len > new_word->word_len){
                    break;
                }
                else{
                    // Go to the next node in the list
                    prev = crt;
                    crt = crt->next;
                }
            }

            // At this point we have found the previous node after which
            // we should add our word.
            // If prev is NULL it means that the new word should go in
            // as the new head of the list. Since there is no function
            // in the library to do this, we do it manually
            if (prev == NULL){
                struct node *new_node = init_list(new_word);
                new_node->next = word_list;
                word_list = new_node;
            }else{
                // If prev has a value we just use the add_after function
                // from the linked list library
                struct node *new_node = add_after(prev, new_word);
            }
        }
    }

    // Now print the words in the list.
    struct node *crt;
    for (crt = word_list;crt != NULL; crt=crt->next){
        struct word *crt_word = (struct word*)crt->content;
        printf("%s: %d\n", crt_word->word, crt_word->word_len);
        free(crt_word); // Free the memory for the word
    }
    clear(word_list); // Free the memory for the list
}

