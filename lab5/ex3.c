#include <stdio.h>
#include <string.h>

/*
 * Converts a string of digits to an integer.
 * The integer is stored at the dest pointer.
 * Returns -1 in case of error, 0 on success.
 */
int string_to_int(const char *string, int *dest){
    *dest = 0;
    for (;*string != '\0';string++)
        if (*string >= '0' && *string <= '9')
            *dest = (*dest*10)+(*string-'0');
        else
            return -1; // Format error, string contains non-digit
    return 0;
}

void main(int argc, char *argv[]){
    int int1, int2;
    char operator;
    int res;

    if (argc < 4){ // Check the number of command line parameters
        printf("Usage: %s number operator number\n\t\tOperator is one of +-*/\n",
                argv[0]);
        return;
    }

    if (string_to_int(argv[1], &int1) == -1){
        printf("Format error parsing %s\n", argv[1]);
        return;
    }
    operator = argv[2][0]; // argv[2] is a string, the operator is the first char
    if (string_to_int(argv[3], &int2) == -1){
        printf("Format error parsing %s\n", argv[3]);
        return;
    }

    switch (operator){
        case '+': res = int1+int2; break;
        case '-': res = int1-int2; break;
        case '*': res = int1*int2; break;
        case '/': res = int1/int2; break;
        defaut: // Any other operator characters are not supported
            printf("Usage: %s number operator number\n\t\tOperator is one of +-*/\n",
                    argv[0]);
            return;
    }

    printf("= %d\n", res);
}
