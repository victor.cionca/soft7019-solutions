#include "option_parser.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct parser * init_option_parser(){
    struct parser *p = malloc(sizeof(struct parser));
    p->option_list = NULL;
    return p;
}

int string_to_int(const char *string, int *val){
    *val = 0;
    for (;*string != '\0';string++)
        if (*string >= '0' && *string <= '9')
            *val = *val*10 + (*string-'0');
        else
            return -1; // Format error
}

void print_options(struct parser *p){
    struct node *crt = p->option_list;
    while (crt != NULL){
        struct option *o = (struct option*)crt->content;
        printf("%s. long: %s short: %c type: %d\n", o->name, o->optlong,
                            o->optshort, o->type);
        crt = crt->next;
    }
}

void add_option(struct parser *p, char *optlong, char optshort, char *name,
                int argument_type){
    // Set up a struct option structure
    struct option *o = malloc(sizeof(struct option));
    strcpy(o->optlong, optlong);
    o->optshort = optshort;
    strcpy(o->name, name);
    o->type = argument_type;

    // Add the new option to the list
    if (p->option_list == NULL){
        p->option_list = init_list(o);
    }else{
        append(p->option_list, o);
    }
}

enum{
    FIND_LONG,
    FIND_SHORT,
    FIND_NAME,
};

/**
 * This searches through the list of options for the one that matches the
 * given field name specified by lookup_field. This is defined as one of the
 * constants in the above enum.
 * To call this function create a struct option, fill in the field of interest,
 * which can be the long option, the short, or the name, and pass both arguments.
 * If the option is not found, NULL is returned.
 */
struct option *find_option(struct parser *p, struct option *opt, int lookup_field){
    struct node *crt = p->option_list;

    while (crt != NULL){
        struct option *crtopt = (struct option*)crt->content;
        switch (lookup_field){
            case FIND_LONG:
                if (strcmp(crtopt->optlong, opt->optlong) == 0) return crtopt;
                break;
            case FIND_SHORT:
                if (crtopt->optshort == opt->optshort) return crtopt;
                break;
            case FIND_NAME:
                if (strcmp(crtopt->name, opt->name) == 0) return crtopt;
                break;
        }
        crt = crt->next;
    }
    return NULL;
}

int parse(struct parser *p, int argc, char *argv[]){
    int i;
    int matched_options = 0;
    /*
     * As we parse the command line arguments we first look for a
     * registered option, which should start with - or --.
     * Then we use the next argument as the option value.
     */
    struct option *crtoption = NULL;

    for (i=1;i<argc;i++){
        if (argv[i][0] == '-'){
            // This must be an option
            struct option lookup; // We will use this to search for the option in the list
            int lookuptype;       // This will show the field type we are looking for
            if (crtoption != NULL && crtoption->type != TYPE_EMPTY){
                printf("Format error: option %s follows an non-empty option\n",
                        argv[i]);
                return -1;
            }
            if (argv[i][1] == '-'){ // This is a long option
                // Search through the option list for the option that matches this
                strcpy(lookup.optlong, &argv[i][2]); // Copy long option from third character onwards
                lookuptype = FIND_LONG;
            }else{
                lookup.optshort = argv[i][1];
                lookuptype = FIND_SHORT;
            }
            // Ready to look for option
            crtoption = find_option(p, &lookup, lookuptype);
            if (crtoption == NULL){
                printf("Option %s not recognised\n", argv[i]);
            }
            if (crtoption != NULL && crtoption->type == TYPE_EMPTY){
                // Set the option flag
                crtoption->value.set = 1;
                // Count the option
                matched_options ++;
                // This option should not be followed by an argument, so reset
                // crtoption to NULL
                crtoption = NULL;
            }
        }else{
            // This must be an argument. Does it have an option before?
            if (crtoption == NULL){
                printf("Format error: no option before argument %s\n", argv[i]);
                return -1;
            }
            // Store the new argument as the option value, based on the option type
            switch (crtoption->type){
                case TYPE_INT:
                    if (string_to_int(argv[i], &crtoption->value.val) == -1){
                        printf("Format error: argument %s expected to be int\n", argv[i]);
                        return -1;
                    }else{
                        // Count the option
                        matched_options ++;
                    }
                    break;
                case TYPE_STRING:
                    crtoption->value.string_val = argv[i];
                    // Count the option
                    matched_options ++;
                    break;
                case TYPE_EMPTY:
                    printf("Format error: argument %s follows an empty option type\n", argv[i]);
                    return -1;
            }
            // After processing the argument, set the crtoption to null again.
            // This way we start again at the following option.
            crtoption = NULL;
        }
    }
    return matched_options;
}

struct option *get_option(struct parser *p, char *name){
    // This is easy. Use find_option to look for the option that matches the name.
    struct option lookup;
    strcpy(lookup.name, name);
    return find_option(p, &lookup, FIND_NAME);
}
