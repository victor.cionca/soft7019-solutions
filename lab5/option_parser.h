#ifndef __OPTION_PARSER_H
#define __OPTION_PARSER_H
#include "linkedlist.h"

enum{
    TYPE_EMPTY,
    TYPE_INT,
    TYPE_STRING,
};

struct option{
    char optlong[10];
    char optshort;
    char name[10];
    int type;
    union{
        char *string_val;
        int val;
        unsigned char set; // This is for empty options that work as flags
    }value;
};

struct parser{
    struct node *option_list;
};

struct parser * init_option_parser();

void add_option(struct parser *p, char *optlong, char optshort, char *name, int argument_type);
int parse(struct parser *p, int argc, char *argv[]);
struct option *get_option(struct parser *p, char *name);

void print_options(struct parser *p);
#endif
