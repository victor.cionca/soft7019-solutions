#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H

/**
 * This is a header file.
 * It contains definitions of types and function prototypes.
 */
struct node {
    void *content; // Linked list supports any type of data
    struct node *next;
};

struct node *init_list(void *data);
struct node *append(struct node *head, void *data);
struct node *add_after(struct node *n, void *data);
void remove_after(struct node *n);
int remove_first(struct node *head, void *data);
void clear(struct node *head);

#endif
