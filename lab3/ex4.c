#include <stdio.h>  // for printf and getchar
#include <stdlib.h> // for malloc
#include <string.h> // for strcpy

int read_text(char *array, int *length){ // Start again from the read_text in ex2
                                         // length is a pointer to store the word length
                                         // Function now returns number of vowels
    char input = 0; // user input
    int num_vowels = 0; // to count the number of vowels

    *length = 0; // initialise length to 0 
                // Since length is a pointer we dereference it to get to
                // the variable located at the memory address stored in length

    while (input != ' ' && input != '\n'){  // Now we stop at a white space
        input = getchar();  // Read user input
        if (input == ' ' || input == '\n') break; // Do not consider the white space
        array[(*length)++] = input; // Add the char to the end of the array
                                    // and increment the end of the array
        // We have to use parantheses because ++ has higher precedence than *.
        // Without parantheses the length address will be incremented first,
        // then dereferenced. This would mean that we are then using another
        // memory cell (the next one) than the one that holds the length.


        if (input == 'a' || input == 'e' || input == 'i'
                || input == 'o' || input == 'u'){
            // User char is a vowel, so increment the vowel count
            num_vowels ++;
        }
    }
    array[*length] = '\0';        // Mark the end of the string with null

    return num_vowels;
}

void main(void){
    char words[10][20]; // 10 words, 20 chars per word
    char *words_dynamic[10]; // 10 words, variable length
    char **words_dynamic2;  // Variable number of words, variable length
    int vowel_count[10];    // Static word array
    int *vowel_count_dyn;   // Dynamic word array
    int num_words;
    int i;

    printf("Number of words:");
    scanf("%d", &num_words);
    getchar(); // Clear the newline from the input buffer because we are using
                // getchar later

    // 1. Static array
    printf("Static word array, fixed length\n");
    printf("Input words:\n");
    for (i=0;i<num_words;i++){
        int vowels;
        int length;
        vowels = read_text(words[i], &length);
        vowel_count[i] = vowels;
    }

    printf("You provided:\n");
    for (i=0;i<num_words;i++){
        printf("%s --> %d\n", words[i], vowel_count[i]);
    }

    // 2. Static array, variable length
    printf("Static word array, variable length\n");
    printf("Input words:\n");
    for (i=0;i<num_words;i++){
        char word[20];  // Define a large enough static char array to read the word
        int length;     // This will hold the length of the word read
        vowel_count[i] = read_text(word, &length); // Read word into static array
        words_dynamic[i] = malloc(length+1); // Allocate memory for the dynamic word
                                     // length+1 to make room for null terminator
        strcpy(words_dynamic[i], word); // Copy the word from the static array
    }

    printf("You provided:\n");
    for (i=0;i<num_words;i++){
        printf("%s --> %d\n", words_dynamic[i], vowel_count[i]);
        free(words_dynamic[i]); // free the memory
    }
 
    // 3. Dynamic array
    printf("Dynamic array\n");
    // First allocate space for the array of pointers to words
    words_dynamic2 = malloc(sizeof(char*)*num_words);
    // Allocate space for the vowel counts
    vowel_count_dyn = malloc(sizeof(int)*num_words);
    printf("Input words:\n");
    for (i=0;i<num_words;i++){
        char word[20];  // Define a large enough static char array to read the word
        int length;     // This will hold the length of the word read
        vowel_count_dyn[i] = read_text(word, &length); // Read word into static array
        words_dynamic2[i] = malloc(length+1); // Allocate memory for the dynamic word
                                     // length+1 to make room for null terminator
        strcpy(words_dynamic2[i], word); // Copy the word from the static array
    }

    printf("You provided:\n");
    for (i=0;i<num_words;i++){
        printf("%s --> %d\n", words_dynamic2[i], vowel_count_dyn[i]);
        free(words_dynamic2[i]); // free the memory
    }
    free(vowel_count_dyn);  // Free the vowel count array
    free(words_dynamic2);   // Free the word pointer array
}
