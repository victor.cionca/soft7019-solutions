#include <stdio.h>

char vowels[5]; // Holds the vowel counts


int read_text(char *array){ // Based on the function in ex 2
    char input = 0; // user input
    int num_chars = 0; // how many characters have been read

    while (input != '\n'){  // Boiler plate
        input = getchar();  // Read user input
        if (input == '\n') break; // Do not consider the newline
        array[num_chars++] = input; // Add the char to the end of the array
                                    // and increment the end of the array
        switch (input){ // Based on the input increment the corresponding vowel count
            case 'a': vowels[0]++;break;
            case 'e': vowels[1]++;break;
            case 'i': vowels[2]++;break;
            case 'o': vowels[3]++;break;
            case 'u': vowels[4]++;break;
        }
    }
    array[num_chars] = '\0';        // Mark the end of the string with null

    return num_chars;
}

void main(void){
    char array[20];     // The array of chars. Max 20 chars. More than 20 => segfault
    int array_len;      // Array length
    int i=0;

    array_len = read_text(array);   // Call the function

    printf("String \"%s\" has %d characters\n", array, array_len);

    // Now print the vowel counts:
    printf("a -> %d\n", vowels[0]);
    printf("e -> %d\n", vowels[1]);
    printf("i -> %d\n", vowels[2]);
    printf("o -> %d\n", vowels[3]);
    printf("u -> %d\n", vowels[4]);

    // We could have done this in a for loop but it would have been more code,
    // see below:
    // for (i=0;i<5;i++){
    //     switch (i){
    //         case 0: printf("a -> %d\n", vowels[0]); break;
    //         case 1: printf("e -> %d\n", vowels[1]); break;
    //         case 2: printf("i -> %d\n", vowels[2]); break;
    //         case 3: printf("o -> %d\n", vowels[3]); break;
    //         case 4: printf("u -> %d\n", vowels[4]); break;
    //     }
    // }
    // As you can see within the for loop we need to treat each index on
    // its own within the switch statement, so the for loop is not needed.
    // What we are doing above without the for loop is called
    // loop unrolling.
}
