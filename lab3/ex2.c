#include <stdio.h>  // for printf and getchar

int read_text(char *array){
    char input = 0; // user input
    int num_chars = 0; // how many characters have been read

    while (input != '\n'){  // Boiler plate
        input = getchar();  // Read user input
        if (input == '\n') break; // Do not consider the newline
        array[num_chars++] = input; // Add the char to the end of the array
                                    // and increment the end of the array
    }
    array[num_chars] = '\0';        // Mark the end of the string with null

    return num_chars;
}

void main(void){
    char array[20];     // The array of chars. Max 20 chars. More than 20 => segfault
    int array_len;      // Array length

    array_len = read_text(array);   // Call the function

    printf("String \"%s\" has %d characters\n", array, array_len);
}
