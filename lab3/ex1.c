// Summing up an array
#include <stdio.h>  // for printf
#include <stdlib.h> // for malloc

int sum_array(int *array, int length){
    int sum = 0; // Sum total, starts at 0
    int i; // Use this as an index in the array

    for (i=0;i<length;i++){
        sum = sum + array[i];   // Add to the sum total the next element in the array
    }

    return sum;
}

void main(void){
    int sum;
    int array1[] = {10,20,30,11,21,31}; // constant array initialiser
    int array2[10]; // Uninitialised, capacity 10 elements
    int *array3;    // Declared as pointer, needs malloc to allocate memory
    int array_len;  // To hold length of array2 and array3
    int i;          // To read the contents of array2 and array3

    // 1. Constant array hard-coded in main
    sum = sum_array(array1, 6);
    printf("Sum of hard-coded array = %d\n", sum);

    // 2. Static array. Max size is 10
    printf("====================================\nStatic array\n");
    // First read the length
    printf("Length of array: ");
    scanf("%d", &array_len); // Address of array_len because scanf works with pointers

    printf("Array elements:");
    // Read the array elements with a for loop
    for (i=0;i<array_len;i++){
        // Read integers into the array elements
        // array2[i] is the element at index i
        // We need to get the element's address with &
        scanf("%d", &array2[i]);
    }
    // Here we call the function straight in the printf. This will be evaluated
    // and replaced with the value returned (the sum).
    printf("Sum of static array is %d\n", sum_array(array2, array_len));

    // 3. Dynamic array, allocated with malloc
    printf("====================================\nDynamic array\n");
    // First read the length
    printf("Length of array: ");
    scanf("%d", &array_len); // Address of array_len because scanf works with pointers

    // Allocate memory for the array with malloc
    // We need to allocate space for array_len integers
    // malloc returns the address of the newly allocated space, which we store
    // in array3
    array3 = malloc(sizeof(int)*array_len);

    printf("Array elements:");
    // Read the array elements with a for loop
    for (i=0;i<array_len;i++){
        // Read integers into the array elements
        // array3[i] is the element at index i
        // We need to get the element's address with &
        scanf("%d", &array3[i]);
    }
    // Here we call the function straight in the printf. This will be evaluated
    // and replaced with the value returned (the sum).
    printf("Sum of dynamic array is %d\n", sum_array(array3, array_len));

}
