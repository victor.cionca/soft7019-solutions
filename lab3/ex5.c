// Subnetting
#include <stdio.h>


// 1. Function to calculate the subnet mask
unsigned char subnet_mask(int num_subnets){
    unsigned char subnet_byte = 0xff;

    /*
     * In order to have num_subnets it means that the first K bits of the subnet
     * byte must be 0. All we need to do is figure out how many bits (K) are
     * needed to represent the number of subnets. This can be done by dividing
     * num_subnets by 2 until it is 0. Divide by 2 can be done by shifting right.
     *
     * At the same time when we divide num_subnets by 2 we shift in a zero
     * into the left of the subnet byte. Since it is initialised with all 1s
     * (0xFF is all 1s) we will have at the end as many 0 bits as is required
     * to represent num_subnets
     */
    while (num_subnets > 0){
        subnet_byte  = subnet_byte >> 1;
        num_subnets >>= 1;
    }

    /*
     * Now the subnet byte has 0 bits in the network part and 1 bits in the host
     * part. To turn it into a mask we need to invert it, using the ~ operator.
     */
    return ~subnet_byte;
}

// 2. Print the IP of a certain host
void host_ip(unsigned char *base_ip, unsigned char subnet_mask, int subnet, int host){
    unsigned char last_byte = subnet;
    /*
     * First we need to move the subnet number (its binary representation) into
     * the subnet part of the last byte. To do this we shift the subnet_mask byte
     * right until it has a 1 in the least significant position. At the same time
     * we shift the subnet number left.
     */
    while ((subnet_mask & 0x01) == 0){
        subnet_mask >>= 1;
        last_byte <<= 1;
    }

    // Now all we need to do is add the host number
    last_byte |= host;

    printf("Ip of host %d on subnet %d is %hhu.%hhu.%hhu.%hhu\n",
            subnet, host,
            base_ip[0], base_ip[1], base_ip[2], last_byte);
}

void main(void){
    unsigned char base[3];
    int subnets;
    unsigned char subnet_byte;
    int subnet_number;
    int host_number;

    printf("Base class C address: ");
    scanf("%hhu.%hhu.%hhu", &base[0], &base[1], &base[2]);

    printf("Subnets needed: ");
    scanf("%d", &subnets);

    subnet_byte = subnet_mask(subnets);
    printf("Subnet mask: 255.255.255.%d\n", subnet_byte);

    printf("Subnet number: "); scanf("%d", &subnet_number);
    printf("Host number: "); scanf("%d", &host_number);

    host_ip(base, subnet_byte, subnet_number, host_number);
}
