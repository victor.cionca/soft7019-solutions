/**
 * Converting strings of digits to numbers.
 * ... and adding 1 to the result.
 *
 * A string of digits is: '3', '4', '1'.
 * The corresponding number is 341.
 *
 * If the digits of a number are a,b,c (above a='3', b='4', c='1'),
 * the resulting number, abc can be represented as
 * a*100 + b*10 + c
 * or, by factoring by 10:
 * c+10*(b + 10*a) = c + 10*(b + 10*(a+10*0))
 *
 * This format allows us to build a number from its digits in 
 * the following steps:
 * 1. Initialise the number N=0
 * 2. Get the next digit d (while there are digits)
 * 3. Shift the number right (multiply by 10)
 * 4. Add the digit.
 * 5. Go back to 2
 *
 * An example. Digits are 3, 4, 1.
 * 1. N=0
 * 2. d = 3
 * 3. N=0*10=0
 * 4. N = N+d = 0+3=3
 * 5. d = 4
 * 6. N=3*10=30 (see how we shifted the 3 one position to the right?)
 * 7. N = N+d = 30+4=34
 * 8. d = 1
 * 9. N=34*10=340
 *10. N = N+d = 340+1=341
 */
#include <stdio.h>

void main(void){
    int N=0; // Initialise the number
    char c=0; // Here we read the digits

    while (c!='\n'){
        c = getchar(); // this is boilerplate code for reading characters
        if (c == '\n') break;

        if (c >= '0' && c <= '9') { // Only consider digits. Ignore other chars
            N = N*10;
            N = N+(c-'0');
        }
    }
    printf("%d\n", N+1);
}
