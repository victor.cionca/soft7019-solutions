/**
 *
 * Changing character case
 *
 * In the ASCII table the upper case characters are at
 * lower values than lower case characters.
 *
 * That's all we need to know.
 * We can then use the fact that C internally converts
 * character constants (ex: 'h') into their corresponding
 * ASCII code. This therefore allows:
 * - comparing chars with character constants (ex. (c > 'a')),
 * - subtracting characters constants from chars (ex. c - 'a')
 * 
 * We determine the difference between lower case and
 * upper case characters as 'a' - 'A' and use that
 * as an offset.
 * If we add the difference to an upper case character
 * it will be converted to lower case.
 */
#include <stdio.h>

void main(void){
    char c = 0;
    char outc;
    int caseDiff = 'a' - 'A';

    printf("Case difference: %d\n", caseDiff);
    while (c != '\n'){
        c = getchar();

        if (c == '\n'){
            printf("\n");
            break;
        }

        if (c >='a' && c <='z') outc = c - caseDiff;
        else if (c >= 'A' && c <= 'Z') outc = c + caseDiff;
        else outc = c;

        printf("%c", outc);
    }
}
