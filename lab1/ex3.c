/**
 * Summing up a list of numbers separated by commas.
 * The whole list is read as a string of characters
 * and then processed one character at a time.
 *
 * Example:
 * 11,21,30
 * Result is 62 (11+21+30).
 *
 * The program will parse the string of characters one at a time
 * using getchar. It will build the numbers as it goes along and
 * add them to a running total (that starts at 0). It must therefore
 * keep track of the running total and the current number.
 *
 * If the character is a digit then it will build up a number by
 * shifting the digits into the right side of the number, as done
 * in ex 2.
 *
 * If the character is a comma or the end of the line (newline)
 * the number that was being built has just been completed so we
 * add it to the running total. Furthermore the current number
 * is reset to 0, so that with the next digit we restart building
 * the number. If we don't reset the number back to zero then the
 * next number will start shifting from the current number and
 * we would get a large number.
 */
#include <stdio.h>

void main(void){
    char input = 0; // character that we are reading (boilerplate)
    int sum = 0; // this is the running total
    int number = 0; // this is the number that is currently read

    while (input != '\n'){
        input = getchar(); // boilerplate so far

        switch (input){ // the system has two states, depending on the char read
            case ',':   // if it is a comma we have just completed a number so...
                sum += number;  // add the number to the running total
                number = 0;     // reset the number
                break;          // break here so we don't bleed into the next state
            case '\n':  // if it is a new line we have finished the sum
                sum += number;  // add the last read number to the sum
                printf("Total=%d\n", sum);  // print the sum
                sum = 0;    // reset the sum
                number = 0; // reset the number
                /* We could easily change the program so that it keeps reading
                 * lists of numbers on each line. We just have to change the
                 * input condition of the while, and maybe add another state,
                 * for example if the character 'x' is provided the program
                 * exits.
                 */
                break;
            default:    // if it is not a newline or a comma
                if ((input >= '0') && (input <= '9')){  // consider only digits
                    number = number * 10 + (input-'0'); // build the number
                }
        }
    }
}
