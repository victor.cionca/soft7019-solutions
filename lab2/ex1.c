/**
 * scanf
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
 * Constants for the matching state, which can be no matching, matching a string
 * or matching an int.
 */
enum{
    MATCH_NO = 0,
    MATCH_STRING, // This is implicitly initialised with 1
    MATCH_INT, // This is implicitly initialised with 2
};

/**
 * Scanf implementation that supports strings and ints.
 *
 * Parameters:
 * format: char array with the format string, containing %d and %s conversion
 *          specifiers
 * arguments: array of pointers to arguments; the pointers WILL BE ALLOCATED
 *          within the function, based on the input type
 * arg_types: array of characters representing the argument types:
 *          'd' for ints and 's' for strings; the index in arg_types
 *          corresponds to that in arguments.
 *
 */
int my_scanf(const char *format, void **arguments, char *arg_types){
    int matching = MATCH_NO;
    int matched = 0;    // number of matched arguments
    int fmt_idx = 0;    // index in format string
    char input = 0;     // user characters
    int string_len = 0; // number of characters in a string argument
    char string_arg[50]; // char array to hold the currently read string;
                         // this is then copied into a newly allocated string
                         // into the arguments array
                         // This allows us to allocate exactly the amount of
                         // memory required by the user input.

    while (format[fmt_idx] != 0){
        input = getchar(); // Read a character from the user

        if (matching){  // If we are matching a conversion specifier...
            switch (matching){
                case MATCH_STRING:
                    if (input == ' ' || input == '\t' || input == '\n'){
                        // matching complete
                        // Terminate the string
                        string_arg[string_len] = '\0';
                        // Allocate memory for the argument
                        arguments[matched] = malloc(string_len);
                        // Copy the completed string into the argument
                        strncpy(arguments[matched], string_arg, string_len+1);
                        fmt_idx ++; // Advance in the format string
                        matched ++; // One more argument matched
                        matching = MATCH_NO;
                    }else{
                        // add to the string
                        string_arg[string_len++] = input;
                        continue;
                    }
                    break;
                case MATCH_INT:
                    if (input >= '0' && input <= '9'){
                        // add to the number
                        // We need to cast the argument pointer to an int
                        /* Explanation:
                         * arguments is an array of pointers.
                         * arguments[matched] is the pointer we just allocated.
                         * Since we are treating the argument as an int
                         * we need to first cast arguments[matched] to an int.
                         * We also need to dereference it.
                         */
                        // Here we also see the effectiveness of the *= or += 
                        // operators. Without these we would have had to have
                        // the variable on the right side as well, which would
                        // make a long and complicated assignment.
                        *((int*)arguments[matched]) *= 10;
                        *((int*)arguments[matched]) += input -'0';
                        continue;
                    }else{
                        // matching complete
                        // Nothing else to do here, the number is complete
                        fmt_idx ++; // Advance in the format string
                        matched ++; // One more argument matched
                        matching = MATCH_NO;
                    }
                    break;
            }
        }

        if (format[fmt_idx] == '%'){ // Conversion specifier encountered
            fmt_idx ++;
            switch (format[fmt_idx]){ // Check the conversion specifier type
                case 'd': // matching number
                    if (input >= '0' && input <= '9'){
                        // Set the current argument's type to integer
                        arg_types[matched] = 'd';
                        // First the number must be allocated in the argument list
                        arguments[matched] = malloc(sizeof(int));
                        // Initialise the current argument with the new digit
                        *((int*)arguments[matched]) = input -'0';
                        matching = MATCH_INT;
                    }else{
                        // Matching error. We return the number of matched args
                        return matched;
                    }
                    break;
                case 's': // matching string
                    if (input != ' ' || input != '\t' || input != '\n'){
                        // There is a match so start building the string
                        // Set the current argument's type to string
                        arg_types[matched] = 's';
                        // Initialise the current argument with the new character
                        string_len = 0;
                        string_arg[string_len++] = input;
                        matching = MATCH_STRING;
                    }else{
                        // Matching error. We return the number of matched args
                        return matched;
                    }
                    break;
            }
        }else{
            // Not matching, not at the start of a conversion specifier,
            // the user input must match the format string exactly
            if (input != format[fmt_idx++]) return matched;
        }
    }

    return matched;
}

/**
 * This function counts the conversion specifiers (occurrences of '%')
 * in a format string
 */
int get_number_of_arguments(const char *format){
    int args = 0;
    int i = 0;

    while (format[i] != '\n'){
        if (format[i] == '%') args++;
        i++;
    }
    return args;
}

/**
 * The main function provides flexibility and does not require hard-coded
 * values, such as the type of the arguments or the number of arguments.
 *
 * The user must provide the format string. From the format string the
 * number of arguments is determined using the get_number_of_arguments function.
 * In main the arguments are managed as pointers, and the memory for each pointer
 * is allocated by scanf as the user input is received.
 * Therefore main will only allocate an array to hold all the pointers.
 * We also need to know the type of each pointer so that we can output it.
 * Remember that a pointer is a memory address, but printf must know how to
 * interpret it, what conversion specifier to use.
 * To this extent we use the array of argument types. This is allocated here in
 * main, however the argument types are set by scanf as the arguments are matched.
 */
void main(void){
    void **arguments; // list of pointers to arguments
    char *arg_types;   // Type of arguments
    int num_args;     // number of arguments
    char format[50]; // format string
    char input = 0; // to read the format string with getchar
    int i = 0; // index in the format string
    int matched = 0;    // How many characters matched

    printf("Format string:");
    // Read format string with getchar so we can take in spaces as well
    while (input != '\n'){
        input = getchar();
        if (input == '\n') break; // Discard the newline
        format[i++] = input;
    }
    format[i] = '\0'; // Terminate the format string

    // How many arguments in the format string?
    num_args = get_number_of_arguments(format);

    // Allocate memory for the pointers to the arguments
    arguments = malloc(sizeof(void*)*num_args);
    // Allocate memory for the argument type array
    arg_types = malloc(num_args);

    printf("Input:");
    // Call scanf
    matched = my_scanf(format, arguments, arg_types);

    // Print the arguments, considering their types
    for (i=0; i< matched; i++){
        printf("Argument %d: ", i);
        switch (arg_types[i]){
            case 'd': // Argument is an int
                printf("%d\n", *(int*)arguments[i]);
                break;
            case 's': // Argument is a string
                printf("%s\n", (char*)arguments[i]);
                break;
        }
        free(arguments[i]); // Also free the memory
    }

    free(arguments); // Free the memory for the arguments
    free(arg_types); // Free the memory for the argument types

}
