/**
 * Searching in sorted lists.
 *
 * Divide and conquer algorithm, recursive implementation.
 */
#include <stdio.h>
#include <stdlib.h>

// This is used by the qsort library function to sort the list
int cmp(const void *arg1, const void *arg2){
    return *(int*)arg1 - *(int*)arg2;
}

// This generates a sorted list of random values. The array must be allocated
// by the user.
void random_sorted_list(int *array, int length){
    int i=0;
    for (i=0;i<length;i++) array[i] = (int)(random()%100);
    qsort(array, length, sizeof(int), cmp);
}

int search(int *array, int value, int start, int end){
    int middle_idx = (start + end)/2;   // Middle of the list

    if (end == start){  // If the list has only one element...
        if (value == array[start]) return start;
        else return -1;
    }
    if (value == array[middle_idx]) // If we find the value at the middle element
        return middle_idx;
    if (value < array[middle_idx])  // The value must be in the first half
        return search(array, value, start, middle_idx-1);
    if (value > array[middle_idx])  // The value must be in the second half
        return search(array, value, middle_idx+1, end);
}

void main(void){
    int list_length;
    int *list;
    int value; // value to search for in the list
    int i;

    // Read the list length
    printf("List length:");
    scanf("%d", &list_length);

    // Allocate memory for the list
    list = malloc(sizeof(int)*list_length);

    // Generate the list values using the given function
    random_sorted_list(list, list_length);

    // Print the sorted list
    for (i=0; i<list_length; i++) printf("%d,", list[i]);
    printf("\n");

    printf("Value to look for:");
    scanf("%d", &value);

    i = search(list, value, 0, list_length-1);
    if (i == -1) printf("Couldn't find value\n");
    else printf("Value %d found at index %d\n", value, i);
}
