#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(int argc, char *argv[]){
    FILE *src, *dst;
    FILE *f;
    char code[26];
    char buf[20];

    // Open the source and destination files, if available
    if (argc < 3){
        printf("Usage: %s source destination\n", argv[0]); // argv[0] is the binary
        return;
    }

    src = fopen(argv[1], "r"); // Open the source file
    if (!src){
        printf("Source file %s cannot be found\n", argv[1]);
        return;
    }

    dst = fopen(argv[2], "w"); // Open the destination file for writing
    if (!dst){
        printf("Error opening destination file %s\n", argv[2]);
        return;
    }

    /*
     * Load the code
     * This contains lines
     * %c:%c
     * This is two characters, separated by ':' (which is ignored)
     * In the code, the character on the left should be replaced by that on 
     * the right.
     */
    f = fopen("code.dat", "r");
    if (!f){
        printf("Error opening code.dat\n");
        return;
    }

    while (1){
        int read = 0;
        char src, dst;
        read = fscanf(f, "%c:%c\n", &src, &dst); // Formatted read from the file
        if (read == EOF) break;
        code[src-'a'] = dst;
    }

    fclose(f);

    // Encode the source file into the destination
    while (fgets(buf, 20, src)){
        int i;
        // Replace the characters in the buffer
        /*
         * buf[i] is a letter read from the file
         * buf[i] - 'a' is the index of that letter in the alphabet
         * we can use that index to retrieve from the code array the
         * conversion of the character.
         */
        for (i=0; i<strlen(buf); i++)
            if (buf[i] >= 'a' && buf[i] <= 'z') buf[i] = code[buf[i]-'a'];
        // Write the result to the destination file
        fputs(buf, dst);
    }

    fclose(src);
    fclose(dst);
}
