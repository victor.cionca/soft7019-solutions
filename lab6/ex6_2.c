#include <stdio.h>
#include <stdlib.h>

/**
 * Convert a string to an int.
 * Return -1 in case the string contains non-numeric characters.
 * Return 0 on success.
 */
int string_to_int(const char *string, int *dst){
    *dst = 0;
    for (;*string!='\0';string++)
        if (*string >= '0' && *string <= '9')
            *dst = *dst*10 + (*string - '0');
        else
            return -1;
    return 0;
}

void main(int argc, char *argv[]){
    FILE *db;
    int average;
    int read = 0;

    if (argc < 2){
        printf("Usage: %s average\n", argv[0]);
        return;
    }

    // Read in the average from the command line, converting to int
    if (string_to_int(argv[1], &average) == -1){
        printf("Argument 1 (%s) must be a number between 0-100\n", argv[1]);
        printf("Usage: %s average\n", argv[0]);
        return;
    }

    if ((db = fopen("student_db.txt", "r")) == NULL){ // Open file for READING
        printf("Error opening student database in append mode\n");
        return;
    }

    while (read != EOF){
        char name[20];
        int year;
        char group;
        int avg;
        read = fscanf(db, "%s %d %c %d\n", name, &year, &group, &avg);
        if (avg > average){
            printf("Name: \t%s\nYear: \t%d\nGroup: \t%c\nAverage: \t%d\n\n",
                    name, year, group, avg);
        }
    }
    fclose(db);
}
