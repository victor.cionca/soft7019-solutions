#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(int argc, char *argv[]){
    FILE *src;
    FILE *f;
    char code[128] = {0}; // The code contains non-alphanumeric characters so 
                          // we cannot use ASCII conversion tricks. 
                          // We have to reserve an array big enough to hold all
                          // the ASCII code.
    char buf[20];

    // Open the source and destination files, if available
    if (argc < 2){
        printf("Usage: %s source\n", argv[0]); // argv[0] is the binary
        return;
    }

    src = fopen(argv[1], "r"); // Open the source file
    if (!src){
        printf("Source file %s cannot be found\n", argv[1]);
        return;
    }

    /*
     * Load the code
     * This contains lines
     * %c:%c
     * This is two characters, separated by ':' (which is ignored)
     * In the code, the character on the left should be replaced by that on 
     * the right.
     */
    f = fopen("code.dat", "r");
    if (!f){
        printf("Error opening code.dat\n");
        return;
    }

    /*
     * Process the code file the same way as in the encoder.
     * However the source file will contain characters on the right of ':'
     * and they must be translated to the characters on the left.
     */
    while (1){
        int read = 0;
        char src, dst;
        /*
         * Formatted read from the file.
         * Note the \n at the end. This is needed because we are reading chars
         * and newline is also a char.
         */
        read = fscanf(f, "%c:%c\n", &src, &dst); // Formatted read from the file
        if (read == EOF) break;
        code[dst] = src;
    }

    fclose(f);

    /*
     * The code doesn't include the space and the newline characters
     * which means they will be transformed into null terminators and
     * ignored.
     * We need to add these manually to the code.
     */
    code['\n'] = '\n';
    code[' '] = ' ';

    while (fgets(buf, 20, src)){
        int i;
        // Replace the characters in the buffer
        /*
         * buf[i] is an encoded symbol read from the file
         */
        for (i=0; i<strlen(buf); i++)
            buf[i] = code[buf[i]];
        // Write the result to the destination file
        fputs(buf, stdout);
    }

    fclose(src);
}
