#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct student{
    char name[20];
    int year;
    char group;
    int average;
};

void main(int argc, char *argv[]){
    FILE *db;
    struct student s;
    int read;

    // Open the student database
    db = fopen("students.db", "r");
    if (!db){
        printf("Could not open students.db\n");
        return;
    }

    while (fread(&s, sizeof(struct student), 1, db) == 1){
        printf("Student: %s %d %c %d\n", s.name, s.year, s.group, s.average);
    }
    fclose(db);
}
