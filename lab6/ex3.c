#include <stdio.h>
#include <stdlib.h>

void main(int argc, char *argv[]){
    FILE *src, *dst;
    char buf[10];

    if (argc < 3){
        printf("Usage: %s source destination\n", argv[0]);
        return;
    }

    if ((src = fopen(argv[1], "r")) == NULL){ // Open in READ mode
        printf("Cannot open source file %s\n", argv[1]);
        return;
    }
    if ((dst = fopen(argv[2], "w")) == NULL){ // Open in WRITE mode
        printf("Cannot open source file %s\n", argv[1]);
        return;
    }

    while (fgets(buf, 10, src)){ // Keep reading until we get a NULL
        fputs(buf, dst);    // Put the buffer into the destination file
    }
}

