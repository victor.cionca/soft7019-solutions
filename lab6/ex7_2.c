#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct student{
    char name[20];
    int year;
    char group;
    int average;
};

void main(int argc, char *argv[]){
    FILE *db;
    struct student s;
    int index;

    if (argc < 2) {
        printf("Usage: %s index\n", argv[0]);
        return;
    }

    index = atoi(argv[1]);

    // Open the student database
    db = fopen("students.db", "r");
    if (!db){
        printf("Could not open students.db\n");
        return;
    }

    fseek(db, sizeof(struct student)*index, SEEK_SET);
    if (fread(&s, sizeof(struct student), 1, db) < 1){
        printf("Could not read index %d\n", index);
        return;
    }else{
        printf("Student %d: %s %d %c %d\n", index, s.name, s.year, s.group, s.average);
    }
    fclose(db);
}
