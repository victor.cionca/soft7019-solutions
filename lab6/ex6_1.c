#include <stdio.h>
#include <stdlib.h>

void main(int argc, char *argv[]){
    FILE *db;

    if (argc < 5){
        printf("Usage: %s name year group average\n", argv[0]);
        return;
    }

    if ((db = fopen("student_db.txt", "a")) == NULL){ // Open in APPEND mode
        printf("Error opening student database in append mode\n");
        return;
    }

    // Use ' ' (whitespace) as separator. Other characters (e.g. comma)
    // would be merged into the %s when reading back from the file with
    // fscanf.
    fprintf(db, "%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);

    fclose(db);
}
