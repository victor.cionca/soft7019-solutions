#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct student{
    char name[20];
    int year;
    char group;
    int average;
};

void main(int argc, char *argv[]){
    FILE *db;
    struct student s;

    if (argc < 5) {
        printf("Usage: %s name year group average\n", argv[0]);
        return;
    }

    // Build the student structure from the parameters
    strncpy(s.name, argv[1], 19);
    s.name[19] = '\0';
    s.year = atoi(argv[2]); // Convert array of chars (a) to int (i)
    s.group = argv[3][0]; // argv[3] is a string, we get the first char
    s.average = atoi(argv[4]);

    // Open the student database
    db = fopen("students.db", "a");
    if (!db){
        printf("Could not open students.db\n");
        return;
    }

    // Append the new student record to the database
    fwrite(&s, sizeof(struct student), 1, db);
    fclose(db);
}
