#include <stdio.h>
#include <stdlib.h>

void main(int argc, char *argv[]){
    FILE *f;

    f = fopen("ex1.dat", "a");

    if (argc >= 2){
        int i;
        for (i=1; i<argc; i++){
            fputs(argv[i], f); // Write command line argument i to the file
            fputs("\n", f);    // Write a new line so we have one word per line
        }
    }else{
        char tmp[21];
        fgets(tmp, 20, stdin); // Read from stdin (keyboard) up to 20 chars into tmp
        fputs(tmp, f);  // Write the user input to file
        fputs("\n", f);
    }

    fclose(f);
}
